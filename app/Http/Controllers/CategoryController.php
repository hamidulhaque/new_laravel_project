<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->paginate(10);
        return view('backend/categories/index', compact('categories'));
    }

    public function show($id = null)
    {
        $category = Category::findOrFail($id);
        return view('backend/categories/view', compact('category'));
        //diffforHuman
    }



    public function create()
    {   
        return view('backend/categories/create');
    }

    public function store(CategoryRequest $request)
    {
        try {

            Category::create($request->all());

            return redirect()->route('admin.categories');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        
        }
    }
    
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('backend/categories/edit', compact('category'));
    }
    // {id}/edit

    public function update(CategoryRequest $request, $id)
    {
        try {
          

            Category::findOrFail($id)->update($request->all());
            // $category = category::findOrFail($id);
            // $category->update($request->all());
            return redirect()->route('admin.categories')->with('message', 'Successfully Upadated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy($id)
    {
        Category::findOrFail($id)->delete();

        return redirect()->route('admin.categories')->with('message', 'Successfully Deleted!');
    }
}
