<x-backend.layouts.master>
    <style>

    </style>


    <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
            <div class="col-12">
                <h2 class="tm-block-title d-inline-block">View category</h2>
            </div>
        
        </div>
        <div class="row tm-edit-category-row">
            <div class="col-xl-6 col-lg-6 col-md-12">
                <div class="row">
                    <div style="display: inline">
                        <h4>title: </h4> {{ $category->title }}
                    </div>
                    <div style="display: inline">
                        <h4>Description: </h4> {{ $category->description }}
                    </div>
                    <div style="display: inline">
                        <h4>Price: </h4> {{ $category->price }}
                    </div>


                </div>


                <div style="display: inline">
                    <h4>created at: </h4> {{ $category->created_at->diffForHumans() }}
                </div>
                <div>
                    <a class="btn btn-outline-info" href="{{ route('admin.categories') }}"> back to categories list</a>
                    <a class="btn btn-outline-warning"
                        href="{{ route('admin.categories.edit', ['id' => $category->id]) }}"> edit</a>

                </div>

            </div>
        </div>

</x-backend.layouts.master>
