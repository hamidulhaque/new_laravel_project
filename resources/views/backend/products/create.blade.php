<x-backend.layouts.master>


    <div class="container py-4">

        <section class="panel panel-default">
            <div class="panel-heading ">
                <div class="row d-flex">
                    <div class="col-md-6">
                        <h3 class="panel-title">Add a product</h3>
                    </div>

                    <div class="col-md-6 justify-contentend">
                        <a href="{{ route('admin.products') }}"><button class=" btn btn-outline-primary">View
                                list</button></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <form action="{{ route('admin.products.store') }}" method="POST" class="form-horizontal" role="form" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <!-- form-group // -->
                 @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                            </ul>
                @endforeach
                    
            </div>
            @endif 
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Product Title</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="title" id="name" placeholder="Product title" value="{{old('title')}} ">
                    {{-- @error('title')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror --}}
                </div>
            </div> <!-- form-group // -->

            <div class="form-group">
                <label for="about" class="col-sm-3 control-label">Product description</label>
                <div class="col-sm-9">

                    <textarea class="form-control"  id="mytextarea" name="description">{{old('description')}}</textarea>
                    {{-- @error('description')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror --}}
                </div>


            </div> <!-- form-group // -->
            <div class="row d-flex">
                <div class="form-group col-sm-3">
                    <label for="qty" class="col-sm-3 control-label">Price</label>
                    <div class="">
                        <input type="text" class="form-control" name="price" id="qty" placeholder="Product price" value="{{old('price')}}">
                        {{-- @error('price')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror --}}
                    </div>
                </div> <!-- form-group // -->

                <div class="form-group col-sm-3">
                    <label for="category">Select a category:</label>
                    <select class="form-select" name="category_id">

                        <option selected>Categories</option>
                        @foreach ($categories as $key=>$value)
                            <option  value="{{ $key }}">{{ $value }}</option>
                        @endforeach


                    </select>
                </div>
                <div class="form-group col-sm-3">
                   
                    <label class="form-label" for="customFile">Product Photo</label>
                    <input type="file" name="image" class="form-control" id="customFile" />
                </div> <!-- form-group // -->
                

            </div> <!-- form-group // -->



    </div>

    <div class="form-group">

        <div class="col-sm-offset-3 col-sm-9">
            <hr>
            <button type="submit" class="btn btn-primary " style="display: block;
                                    width: 100%;
                                    cursor: pointer;
                                    text-align: center;">
                Add</button>
        </div>
    </div> <!-- form-group // -->
    </form>
    </div><!-- panel-body // -->
    </section><!-- panel// -->
    </div> <!-- container// -->
 <script>
      tinymce.init({
        selector: '#mytextarea'
      });
</script>
</x-backend.layouts.master>