<x-backend.layouts.master>
    <style>

    </style>


    <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
            <div class="col-12">
                <h2 class="tm-block-title d-inline-block">View Product</h2>
            </div>
        
        </div>
        <div class="row tm-edit-product-row">
            <div class="col-xl-6 col-lg-6 col-md-12">
                <div class="row">
                    <div style="display: inline">
                        <h4>title: </h4> {{ $product->title }}
                    </div>
                    <div style="display: inline">
                        <h4>Description: </h4> <p>{{!!$product->description!!}}</p>
                    </div>
                    <div style="display: inline">
                        <h4>Price: </h4> {{ $product->price }}
                    </div>


                </div>

                <div class="tm-product-img-edit mx-auto">
                    <img src="{{ asset('storage/products/' . $product->image) }}" alt="No image"
                        class="img-fluid d-block mx-auto">
                    <i class="fas fa-cloud-upload-alt tm-upload-icon"
                        onclick="document.getElementById('fileInput').click();"></i>
                </div>
                <div style="display: inline">
                    <h4>created at: </h4> {{ $product->created_at->diffForHumans() }}
                </div>
 
            <div>Created by: {{$product->createdBy->name??'n/a '}}</div>
                <div>
                    <a class="btn btn-outline-info" href="{{ route('admin.products') }}"> back to products list</a>
                    <a class="btn btn-outline-warning"
                        href="{{ route('admin.products.edit', ['id' => $product->id]) }}"> edit</a>

                </div>

            </div>
        </div>

</x-backend.layouts.master>
