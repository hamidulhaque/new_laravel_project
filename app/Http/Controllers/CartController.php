<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function store(Request $request, Product $product)
    {

        if (!Auth::check()) return redirect()->route('login');
        
        $cartItem = Cart::where('added_by', Auth::id())
                        ->where('product_id', $product->id)
                        ->first();

        if ($cartItem) {
            $updateQty = $cartItem['qty']+ $request->qty;
            $cartItem->update(['qty' => $updateQty]);
        } else {
            $data['added_by'] = Auth::id();
            $data['unit_price']= $product->price;
            $data['product_id']=$product->id;
            $data['qty'] = $request->qty;
            Cart::create($data);
        }

        return redirect()->back();
    }

    public function bag()
    {
        $cart = new Cart();
        $cart->sameProduct();

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $cartItems = Cart::where('added_by', Auth::id())->get();

        return view('frontend/cart-page', compact('cartItems'));
    }
}
