<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestValidation;
use App\Models\Category;
use App\Models\Product;
use Image;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Contracts\Service\Attribute\Required;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Exports\UsersExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(10);
        return view('backend/products/index', compact('products'));
    }

    public function show($id = null)
    {
        $product = Product::findOrFail($id);
        return view('backend/products/view', compact('product'));
        //diffforHuman
    }

    public function create()
    {
        $categories = Category::pluck('title', 'id')->toArray();
        return view('backend/products/create', compact('categories'));
    }

    public function store(RequestValidation $request)
    {
        $requestData = $request->all();
        try {
            if ($request->hasFile('image')) {
                $file = $request->image;
                $path = "/app/public/products/";
                $filename = time() . '.' . $file->getClientOriginalExtension();
                Image::make($file)->resize(300, 200)->save(storage_path() . $path . $filename);
                
                $requestData['image'] = $filename;
            }

            $requestData['created_by']=Auth::user()->id;

            Product::create($requestData);

            return redirect()->route('admin.products')->with('message', 'Successfully Stored!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    // for image use interverntion image
    // pdf use packegist
    // maatwebsite-excel for excel


    public function edit($id)
    { $categories = Category::pluck('title', 'id')->toArray();
        $product = Product::findOrFail($id);
        return view('backend/products/edit', compact('product','categories'));
    }
    // {id}/edit


    public function update(RequestValidation $request, $id)
    {
        try {

            $requestData = $request->all();
            $product = Product::findOrFail($id);
            $requestData['updated_by']=Auth::user()->id;
            if ($request->hasFile('image')) {

                $file = $request->image;
                $filename = time() . '.' . $file->getClientOriginalExtension();
                Image::make($file)->resize(300, 200)->save(storage_path() . '/app/public/products/' . $filename);
                $requestData['image'] = $filename;
            } else {
                $requestData['image'] = $product->image;
            }
         
            $product->update($requestData);
            // $product = Product::findOrFail($id);
            // $product->update($request->all());
            return redirect()->route('admin.products')->with('message', 'Successfully Upadated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Product $product)
    {
        $product->update(['deleted_by'=>auth()->id()]);
        $product->delete();
        return redirect()->route('admin.products')->with('message', 'Successfully Deleted!');
    }
    public function trash()
    {
        $products = Product::onlyTrashed()->get();

        return view('backend.products.trashed', [
            'products' => $products
        ]);
    }

    public function restore($id)
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->restore();
        return redirect()->back()->withMessage('Successfully Restored!');
    }

    public function delete($id)
    {
        $product = Product::onlyTrashed()->findOrFail($id);
        $product->forceDelete();
        return redirect()->back()->withMessage('Successfully Deleted Permanently!');
    }


    public function pdf()
    {
        $products = Product::latest()->get();
        $pdf = PDF::loadView('backend.products.pdf', compact('products'));
        return $pdf->download('products-list.pdf');
    }
    // public function export() 
    // {
    //     return Excel::download(new UsersExport, 'users.xlsx');
    // }

    



}
