<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top ">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="{{ route('index') }}">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link active" aria-current="page"
                        href="{{ route('index') }}">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="#">About</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">Category</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{route('index')}}">All Products</a></li>
                        <li>
                            <hr class="dropdown-divider" />
                        </li>
                        @foreach($categories as $category)
                        <li><a class="dropdown-item" href="{{route('index',$category->id)}}">{{$category->title}}</a></li>
                        @endforeach
                       
                    </ul>
                </li>
            </ul>
            <a href="{{route('shopping-bag')}}" >
                <button class="btn btn-outline-dark" type="submit">
                    <i class="bi-cart-fill me-1"></i>
                    Cart
                    <span class="badge bg-dark text-white ms-1 rounded-pill">{{$totalCartItem}}</span>
                </button>
            </a>
            @auth
            <a href="{{route('admin')}}" class="btn btn-info ">Dashboard</a>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a class="btn btn-warning m-1" href="route('logout')" onclick="event.preventDefault();
                                            this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </a>
                </form>
               
            @else
                <div class="d-flex" style="padding-left:10px;">
                    <a href="{{ route('login') }}"> <button class="btn btn-outline-dark" type="submit">
                            <i class="bi bi-person-circle"></i>
                            Login

                        </button>
                    </a>
                    <a href="{{ route('register') }}"> <button class="btn btn-outline-dark" type="submit">
                            <i class="bi bi-person-circle"></i>
                            Signup

                        </button>
                    </a>
                </div>
            @endauth

        </div>
    </div>
</nav>


<header class="bg-dark py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="text-center text-white">
            <h1 class="display-4 fw-bolder">Shop in style</h1>
            <p class="lead fw-normal text-white-50 mb-0">With this shop hompeage template</p>
        </div>
    </div>
</header>
