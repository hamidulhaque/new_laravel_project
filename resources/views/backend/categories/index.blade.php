<x-backend.layouts.master>
    <h2>Categories</h2>
    <div style="display: flex;
    justify-content: right;">
      <a href="{{route('admin.categories.create')}}"><button class="btn btn-outline-primary">Add new</button></a>
    </div>
    <div class="row">
      <p class="bg-warning text-center">You have total @php
          $count = \DB::table('categories')->count();
          
      @endphp <b>{{ $count }}</b> categories</p>
  </div>
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<div class="table-responsive">
<table class="table table-striped table-sm">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($categories as $category)
    <tr>
      <td>{{$loop->iteration}}</td>
      <td>{{$category->title}}</td>
      <td>{{Str::limit($category->description,30)}}</td>
      <td>
        <a style="text-decoration: none" href="{{route('admin.categories.view' ,['id'=>$category->id])}}"><button type="button" class="btn btn-outline-success">View</button></a>
        <a style="text-decoration: none" href="{{route('admin.categories.edit' ,['id'=>$category->id])}}"><button type="button" class="btn btn-outline-secondary">Edit</button></a>
       <form style="display: inline" action="{{route('admin.categories.destroy' ,['id'=>$category->id])}}" method="POST">
         @csrf
         @method('delete')
         <button class="btn btn-outline-danger">Delete</button>
       </form>
    
      
      </td>
    </tr>
    @endforeach
   
    
  </tbody>


</table>
{{$categories->links()}}
</x-backend.layouts.master>