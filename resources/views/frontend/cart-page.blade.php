<x-frontend.layouts.master>
    <section class="" style="background-color: #fdccbc;">
        <div class="container h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col">
                    <p><span class="h2">Shopping Cart </span><span
                            class="h4">({{ $cartItems->count() }} item in your cart)</span></p>
                    @php
                        $itemTotalPrice = 0;
                        $TotalPrice = 0;
                        
                    @endphp

                    @foreach ($cartItems as $cart)
                        @php
                            $itemTotalPrice = $cart->qty * $cart->unit_price;
                            $TotalPrice += $itemTotalPrice;
                            
                        @endphp
                        <form action="">
                            <div class="card mb-4">
                                <div class="card-body p-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-2">
                                            <img src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/E-commerce/Products/1.webp"
                                                class="img-fluid" alt="Generic placeholder image">
                                        </div>
                                        <div class="col-md-2 d-flex justify-content-center">
                                            <div>
                                                <p class="small text-muted mb-4 pb-2">Name</p>
                                                <p class="lead fw-normal mb-0">{{ $cart->product->title }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex justify-content-center">
                                            <div>
                                                <p class="small text-muted mb-4 pb-2">Color</p>
                                                <p class="lead fw-normal mb-0"><i class="fas fa-circle me-2"
                                                        style="color: #fdd8d2;"></i>
                                                    pink rose</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex justify-content-center">
                                            <div>
                                                <p class="small text-muted mb-4 pb-2">Quantity</p>
                                                <input type="number" name="qty" onchange="updatePrice()"
                                                    class="form-control" value="{{ $cart->qty }}">
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex justify-content-center">
                                            <div>
                                                <p class="small text-muted mb-4 pb-2">Price</p>
                                                <p class="lead fw-normal mb-0">{{ $cart->product->price }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex justify-content-center">
                                            <div>
                                                <p class="small text-muted mb-4 pb-2">Total</p>
                                                <p class="lead fw-normal mb-0">{{ $itemTotalPrice }}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                    @endforeach


                    <div class="card mb-5">
                        <div class="card-body p-4">

                            <div class="float-end">
                                <p class="mb-0 me-5 d-flex align-items-center">
                                    <span class="small text-muted me-2">Order total:</span> <span
                                        class="lead fw-normal"> {{ $TotalPrice }}</span>
                                </p>
                            </div>

                        </div>
                    </div>

                    <div class="d-flex justify-content-end">
                        <a role="button" href="" class="btn btn-light btn-lg me-2">Continue shopping</a>
                        <button type="button" class="btn btn-primary btn-lg">Place Order</button>
                    </div>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </section>

    <script>
        function updatePrice(){
          alert('changed')
        }
    </script>
</x-frontend.layouts.master>
