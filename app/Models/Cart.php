<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    use HasFactory;
    protected $fillable = ['added_by', 'product_id', 'qty', 'unit_price'];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->hasMany(User::class,'added_by');
    }

    public function sameProduct()
    {
        $user =Auth::id();
        
        $cartExistUser = Cart::find(Auth::id()) ?? null;
        if($cartExistUser){
            $a = 'hoise';
        }
    }
}
