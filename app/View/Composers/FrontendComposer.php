<?php
 
namespace App\View\Composers;

use App\Models\Cart;
use App\Models\Category;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
 
class FrontendComposer
{
   
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {    $totalCartItem=0;
        if(Auth::check()){
            $totalCartItem=Cart::where('added_by',Auth::id())->get()->count();
            // dd($totalCartItem);
        }
        $categories = Category::all();
        $view->with(
            [
                'categories'=> $categories,
                'totalCartItem'=>$totalCartItem,
            ]);
    }
}