<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index($categoryId = null)
    {
        $categories = Category::pluck('title', 'id')->toArray();
        if ($categoryId) {
            $products = Product::where('category_id',$categoryId)->latest()->paginate(8);
        } else {
            $products = Product::latest()->paginate(8);
        }

        return view('welcome', compact('products', 'categories'));
    }
    public function view($id = null)
    {

        // $product = Product::where('id', $id)->first();

        // firstOrFail

        // if(!$product){
        //     abort(404);
        // }
        // $category= Category::  

        $product = Product::findOrFail($id);
        
        // dd($product);
        return view('frontend/product/details', compact('product'));
    }
}
