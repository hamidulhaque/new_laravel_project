<x-backend.layouts.master>


    <div class="container py-4">

        <section class="panel panel-default">
            <div class="panel-heading ">
                <div class="row d-flex">
                    <div class="col-md-6">
                        <h3 class="panel-title">Add New Category</h3>
                    </div>

                    <div class="col-md-6 justify-contentend">
                        <a href="{{ route('admin.categories') }}"><button class=" btn btn-outline-primary">View
                                list</button></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <form action="
                {{ route('admin.categories.store') }}
                " method="POST" class="form-horizontal" role="form" autocomplete="off">
                    @csrf
                    <!-- form-group // -->
                    {{-- @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif --}}
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Category Title</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="title" id="name" placeholder="Product title"
                                value=" {{old('title')}} ">
                            @error('title')
                                <div class="alert alert-danger">{{$message}}</div>                                    
                                @enderror
                        </div>
                    </div> <!-- form-group // -->

                    <div class="form-group">
                        <label for="about" class="col-sm-3 control-label">Product description</label>
                        <div class="col-sm-9">

                            <textarea class="form-control" name="description">
                              {{{ old('description') }}}
                            </textarea>
                            @error('description')
                          <div class="alert alert-danger">{{$message}}</div>                                    
                          @enderror
                        </div>
                    </div> <!-- form-group // -->

                    <div class="form-group">

                        <div class="col-sm-offset-3 col-sm-9">
                            <div class="d-grid gap-2 py-3">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div><!-- panel-body // -->
        </section><!-- panel// -->


    </div> <!-- container// -->

</x-backend.layouts.master>
