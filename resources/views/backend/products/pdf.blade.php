<style>
    table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
<h5>Products List of E-commerce</h5>
<h3>{{ date('D-M-Y')}}</h3>
<table >
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Price</th>
            <th scope="col">Category</th>
            <th scope="col">Description</th>
        </tr>
    </thead>
    <tbody>
     
        @foreach ($products as $product)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $product->title }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->category->title ?? 'N/A' }}</td>
                <td>{{ Str::limit($product->description, 30) }}</td>
            </tr>
        @endforeach


    </tbody>

  
</table>
<br>
<br>
 <br>
<p>...............</p>
<p>signature</p>