<?php

namespace App\Providers;

use App\Models\Product;
use App\Models\User;
use App\Policies\ProductPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin.products.edit', [ProductPolicy::class ,'update']);
        Gate::define('admin.products.view', [ProductPolicy::class ,'view']);
        Gate::define('admin.products.destroy', [ProductPolicy::class ,'delete']);
        Gate::define('admin.products.trash', [ProductPolicy::class ,'TrashList']);
        Gate::define('admin.products.trash-restore', [ProductPolicy::class ,'restore']);
        Gate::define('admin.products.delete', [ProductPolicy::class ,'forceDelete']);

        Gate::define('admin.products.create', [ProductPolicy::class ,'create']);







    }
}
