<?php
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ColorsController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\SizeController;
use FontLib\Table\Type\name;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/shopping-bag', [CartController::class, 'bag'])->middleware(['auth'])->name('shopping-bag');


Route::post('/product/{product}/comment', [CommentController::class, 'store'])->middleware(['auth'])->name('product-comment');

Route::post('/shopping-bag/{product}/add-to-cart', [CartController::class, 'store'])->middleware(['auth'])->name('upadate-cart');


Route::get('/product/{id}', [FrontendController::class, 'view'])->name('product.details');

require __DIR__.'/auth.php';

Route::prefix('admin')->middleware(['auth'])->group(function () {

    Route::get('/', function () {
        return view('backend/index');
    })->name('admin');
    Route::get('/my',function(){
        dd(auth()->user()->profile);
    })->name('my-profile');
    Route::get('/products', [ProductController::class, 'index'])->name('admin.products');
    Route::get('/products/create', [ProductController::class, 'create'])->name('admin.products.create');
    Route::post('/products/store', [ProductController::class, 'store'])->name('admin.products.store');
    Route::get('/products/export-pdf', [ProductController::class, 'create'])->name('admin.products.pdf');

    Route::get('/products/{id}', [ProductController::class, 'show'])->name('admin.products.view');
    Route::get('/products/{id}/edit', [ProductController::class, 'edit'])->name('admin.products.edit');
    Route::patch('/products/{id}', [ProductController::class, 'update'])->name('admin.products.update');
    Route::delete('/products/{product}', [ProductController::class, 'destroy'])->name('admin.products.destroy');
    Route::get('/products-trash', [ProductController::class, 'trash'])->name('admin.products.trash');
    Route::get('/products-trash/{id}', [ProductController::class, 'restore'])->name('admin.products.trash-restore');
    Route::delete('/products-trash/{id}', [ProductController::class, 'delete'])->name('admin.products.delete');

    // Route::resource('products', ProductController::class);


    Route::get('/categories', [CategoryController::class, 'index'])->name('admin.categories');
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('admin.categories.create');
    Route::post('categories/store', [CategoryController::class, 'store'])->name('admin.categories.store');
    Route::get('/categories/{id}', [CategoryController::class, 'show'])->name('admin.categories.view');
    Route::get('/categories/{id}/edit', [CategoryController::class, 'edit'])->name('admin.categories.edit');
    Route::patch('/categories/{id}', [CategoryController::class, 'update'])->name('admin.categories.update');
    Route::delete('/categories/{id}', [CategoryController::class, 'destroy'])->name('admin.categories.destroy');
});




Route::get('/{categoryId?}', [FrontendController::class, 'index'])->name('index');



