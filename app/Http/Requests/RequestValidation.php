<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $imageValidation='required|mimes:jpeg,jpg,png,gif|max:10000';
        if ($this->Method('patch')) {
            $imageValidation='mimes:jpeg,jpg,png,gif|max:10000';
        }
        
        $title='required|max:30|unique:products,title';
        if($this->Method('post')){
            $title='required|max:30';
        }
        return [
            'title' => $title,
            'description' => 'required|min:20|max:600',
            'price' => 'required|numeric|min:7',
            'image' => $imageValidation,
            'category_id'=> 'required',
        ];
    }
}
