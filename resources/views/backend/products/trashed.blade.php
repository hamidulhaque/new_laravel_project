<x-backend.layouts.master>
    <h2>Trash List</h2>
    <div style="display: flex;
    justify-content: right;">
        <a href="{{ route('admin.products') }}"><button class="btn btn-outline-primary">All Products</button></a>
    </div>
    <div class="table-responsive">
      
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Price</th>
                    <th scope="col">Description</th>
                    <th scope="col">Deleted by</th>
                    <th scope="col">Deleted at</th>

                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ Str::limit($product->description, 30) }}</td>
                        <td>{{$product->deletedBy->name??'No record'}}</td>
                        <td>{{$product->deleted_at->diffForHumans()}}</td>



                        <td>
                            <a style="text-decoration: none"
                                href="{{ route('admin.products.trash-restore', ['id' => $product->id]) }}"><button type="button"
                                    class="btn btn-outline-success">Restore</button></a>
                            
                            <form action="{{route('admin.products.delete',['id'=>$product->id])}}" method="POST" style="display: inline">
                                @csrf
                                @method('delete')
                                <button class="btn btn-outline-danger" type="submit" onclick="return confirm('are you sure to delete? this action cant undo');">Delete</button>
                            </form>


                        </td>
                    </tr>
                @endforeach


            </tbody>


        </table>
     
</x-backend.layouts.master>
