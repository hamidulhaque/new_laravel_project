<x-backend.layouts.master>
    <style>

    </style>


    <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
            <div class="col-12">
                <h2 class="tm-block-title d-inline-block">Edit Product</h2>
            </div>
        </div>
        <div class="row tm-edit-product-row">
          @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            <div class="col-xl-6 col-lg-6 col-md-12">
                <form action="{{ route('admin.products.update', ['id' => $product->id]) }}" enctype="multipart/form-data"
                    method="POST" class="tm-edit-product-form">
                    @csrf
                    @method('PATCH')
                    <div class="form-group mb-3">
                        <label for="name">Product Name
                        </label>
                        <input id="name" name="title" type="text" class="form-control validate"
                            value="{{ old('title', $product->title) }}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="price">Price in tk
                        </label>
                        <input id="price" name="price" type="number" class="form-control validate"
                            value="{{ old('price', $product->price) }}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="description">Description</label>
                        <textarea class="form-control validate tm-small" rows="5" name="description"
                            required="">{{ old('description', $product->description) }}</textarea>
                    </div>
                    <div class="form-group mb-3">
                        <label for="category">Category</label>
                        <select class="custom-select tm-select-accounts" id="category" name="category_id">
                            @foreach ($categories as $key=>$category)
                        <option class="form-control" value="{{$key}}" {{ $product->category_id == $key ? 'selected' : '' }}>{{$category}}</option>
                        @endforeach

                        </select>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 mx-auto mb-4">
                        <div class="tm-product-img-edit mx-auto">
                            <img src="{{ asset('storage/products/' . $product->image) }}" alt="Product image"
                                class="img-fluid d-block mx-auto">
                            <i class="fas fa-cloud-upload-alt tm-upload-icon"
                                onclick="document.getElementById('fileInput').click();"></i>
                        </div>
                        <div class="custom-file mt-3 mb-3">
                            <input id="fileInput" type="file" name="image" style="display:none;">
                            <input type="button" class="btn btn-primary btn-block mx-auto" value="CHANGE IMAGE NOW"
                                onclick="document.getElementById('fileInput').click();">
                        </div>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">Update Now</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</x-backend.layouts.master>
