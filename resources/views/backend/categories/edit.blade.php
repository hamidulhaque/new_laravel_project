<x-backend.layouts.master>
    <style>

    </style>


    <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
            <div class="col-12">
                <h2 class="tm-block-title d-inline-block">Edit category</h2>
            </div>
        </div>
        <div class="row tm-edit-category-row">
          @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            <div class="col-xl-6 col-lg-6 col-md-12">
                <form action="{{ route('admin.categories.update', ['id' => $category->id]) }}" enctype="multipart/form-data"
                    method="POST" class="tm-edit-category-form">
                    @csrf
                    @method('PATCH')
                    <div class="form-group mb-3">
                        <label for="name">category Name
                        </label>
                        <input id="name" name="title" type="text" class="form-control validate"
                            value="{{ old('title', $category->title) }}">
                    </div>
                    
                    <div class="form-group mb-3">
                        <label for="description">Description</label>
                        <textarea class="form-control validate tm-small" rows="5" name="description"
                            required="">{{ old('description', $category->description) }}</textarea>
                    </div>
                    
                    <div class="row">
                        <div class="form-group mb-3 col-xs-12 col-sm-6">
                            <label for="expire_date">Expire Date
                            </label>
                            <input id="expire_date" type="text" value="22 Oct, 2020"
                                class="form-control validate hasDatepicker" data-large-mode="true">
                        </div>
                        <div class="form-group mb-3 col-xs-12 col-sm-6">
                            <label for="stock">Units In Stock
                            </label>
                            <input id="stock" type="text" value="19,765" class="form-control validate">
                        </div>
                    </div>

                   
                        
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block text-uppercase">Update Now</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</x-backend.layouts.master>
