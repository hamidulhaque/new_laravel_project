<x-frontend.layouts.master>
    <style>
        body {
            background-color: #000
        }

        .card {
            border: none
        }

        .product {
            background-color: #eee
        }

        .brand {
            font-size: 13px
        }

        .act-price {
            color: red;
            font-weight: 700
        }

        .dis-price {
            text-decoration: line-through
        }

        .about {
            font-size: 14px
        }

        .color {
            margin-bottom: 10px
        }

        label.radio {
            cursor: pointer
        }

        label.radio input {
            position: absolute;
            top: 0;
            left: 0;
            visibility: hidden;
            pointer-events: none
        }

        label.radio span {
            padding: 2px 9px;
            border: 2px solid #ff0000;
            display: inline-block;
            color: #ff0000;
            border-radius: 3px;
            text-transform: uppercase
        }

        label.radio input:checked+span {
            border-color: #ff0000;
            background-color: #ff0000;
            color: #fff
        }

        .btn-danger {
            background-color: #ff0000 !important;
            border-color: #ff0000 !important
        }

        .btn-danger:hover {
            background-color: #da0606 !important;
            border-color: #da0606 !important
        }

        .btn-danger:focus {
            box-shadow: none
        }

        .cart i {
            margin-right: 10px
        }

    </style>

    <div class="container mt-5 mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="images p-3">
                                <div class="text-center p-4"> <img id="main-image"
                                        src="{{ asset('storage/products/' . $product->image) }}" width="250" /> </div>
                                <div class="thumbnail text-center">
                                    <img onclick="change_image(this)"
                                        src="{{ asset('storage/products/' . $product->image) }}" width="70">
                                    <img onclick="change_image(this)"
                                        src="{{ asset('storage/products/' . $product->image) }}" width="70">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product p-4">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="d-flex align-items-center"> <i class="fa fa-long-arrow-left"></i> <span
                                            class="ml-1">Back</span> </div> <i
                                        class="fa fa-shopping-cart text-muted"></i>
                                </div>
                                <div class="mt-4 mb-3">
                                    <span class="text-uppercase text-muted brand">Category:
                                        <b>{{ $product->category->title ?? 'N/A' }}</b></span>
                                    <br>
                                    <span class="text-uppercase text-muted brand">Orianz</span>

                                    <h5 class="text-uppercase">{{ $product->title }}</h5>
                                    <div class="price d-flex flex-row align-items-center"> <span
                                            class="act-price">{{ $product->price }} tk</span>
                                        <div class="ml-2"> <small class="dis-price"> old price </small>
                                            <span>40% OFF</span>
                                        </div>
                                    </div>
                                </div>


                                {!!$product->description!!}
                                <div class="sizes mt-5">
                                    <h6 class="text-uppercase">Size</h6> <label class="radio"> <input
                                            type="radio" name="size" value="S" checked> <span>S</span> </label> <label
                                        class="radio"> <input type="radio" name="size" value="M">
                                        <span>M</span> </label> <label class="radio"> <input type="radio"
                                            name="size" value="L"> <span>L</span> </label> <label
                                        class="radio"> <input type="radio" name="size" value="XL">
                                        <span>XL</span> </label> <label class="radio"> <input type="radio"
                                            name="size" value="XXL"> <span>XXL</span> </label>
                                </div>
                                <form action="{{ route('upadate-cart', $product->id) }}" method="POST">
                                    @csrf
                                    <div class="cart mt-4 align-items-center">
                                        <div class="col-md-3 col-lg-3 col-xl-3 d-flex">
                                            <label for="qty">Quantity</label>
                                            <input id="qty" name="qty" value="1" type="number" class="">
                                        </div>
                                        <br>
                                        <button class="btn btn-danger text-uppercase mr-2 px-4" type="submit">
                                            Add to cart
                                        </button>
                                        <i class="fa fa-heart text-muted"></i>
                                        <i class="fa fa-share-alt text-muted"></i>


                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">


                    <div class="form-control">
                        @auth
                            <form action="{{ route('product-comment', $product->id) }}" method="POST">
                                @csrf
                                <textarea class="form-control" name="body" id="" rows="5" required>{{ old('body') }}</textarea>
                                <button type="submit" class="btn btn-success">Comment</button>
                            </form>
                        @endauth

                        <h1>Comments</h1>
                        <ul>
                            @foreach ($product->comments as $comment)
                                <li>{{ $comment->body }} <time> <mark>
                                            {{ $comment->created_at->diffForHumans() }}</mark></time>
                                    <br>
                                    Comment by: {{ $comment->commentBy->name }}
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <script>
        function change_image(image) {

            var container = document.getElementById("main-image");

            container.src = image.src;
        }
        document.addEventListener("DOMContentLoaded", function(event) {});
    </script>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</x-frontend.layouts.master>
