<x-backend.layouts.master>
    <h2>All Product</h2>
    <div style="display: flex;
    justify-content: right;">

        @can('admin.products.create')
            <a href="{{ route('admin.products.create') }}"><button class="btn btn-outline-primary">Add new</button></a>
        @endcan

        @can('admin.products.trash')
            <a href="{{ route('admin.products.trash') }}" class="btn btn-outline-dark">Trash Bin</a>
        @endcan
    </div>


    <div class="table-responsive">
        <div class="row">
            <p class="bg-warning text-center">You have total @php
                $count = \DB::table('products')->count();
                
            @endphp <b>{{ $count }}</b> products</p>

        </div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Price</th>
                    <th scope="col">Category</th>
                    <th scope="col">Description</th>

                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $product->title }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->category->title ?? 'N/A' }}</td>
                        
                        <td>{{ Str::limit(strip_tags($product->description), 20) }}</td>
                        <td>
                            @can('admin.products.edit', $product)
                                <a style="text-decoration: none"
                                    href="{{ route('admin.products.view', ['id' => $product->id]) }}"><button
                                        type="button" class="btn btn-outline-success">View</button></a>
                            @endcan

                            @can('admin.products.edit', $product)
                                <a style="text-decoration: none"
                                    href="{{ route('admin.products.edit', ['id' => $product->id]) }}"><button
                                        type="button" class="btn btn-outline-secondary">Edit</button></a>
                            @endcan

                            @can('admin.products.destroy', $product)
                                <form action="{{ route('admin.products.destroy', ['product' => $product->id]) }}"
                                    method="POST" style="display: inline">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger" type="submit"
                                        onclick="return confirm('are you sure to delete?');">Delete</button>
                                </form>
                            @endcan



                        </td>
                    </tr>
                @endforeach


            </tbody>


        </table>
        {{ $products->links() }}
</x-backend.layouts.master>
