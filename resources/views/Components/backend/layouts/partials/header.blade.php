<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{route('admin')}}">Company name</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  <div class="navbar-nav">
    
    <div class="nav-item text-nowrap">



    </div>
    
  </div>
  <div class="dropdown">
    <a class="nav-link text-white dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
     <span><i class="bi bi-person-circle"></i></span> {{Auth::user()->name }} as
    </a>
  
    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink">
      <li><a class="dropdown-item" href="{{route('my-profile')}}">Profile</a></li>
      {{-- <li><a class="dropdown-item" href="#">Another action</a></li> --}}
      <li >
        <form method="POST" action="{{ route('logout') }}">
            @csrf  
            <a class="dropdown-item" href="route('logout')"
                    onclick="event.preventDefault();
                                this.closest('form').submit();">
                {{ __('Log Out') }}
        </a>
        </form></li>
    </ul>
  </div>
</header>
